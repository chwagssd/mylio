# Mylio "Lead Connector"

## Overview
The lead connector is a JavaScript library which can be configured to connect any HTML form to the backend Mylio APIs for **account registration**.

## Sample API Lead Submission (Curl)

```bash
curl 'https://app.mylio.com/account?isTry=true&isReferral=true&callback=TEST123' --data 'email=test%40tes23234234t.com&password=m234234DDDDe&accountname=&sn=&code='
```

## How to use on a website

```html
<html>

<!-- Assuming this is Your Form -->
    <form id="form1">
        Email: <input name="email" id="email_field" type="text" placeholder="Email" /><br />
        
        Password: <input name="password" id="password_field" type="password" />
        
        <input type="submit" value="Register" />
    </form>


<!-- ================== BEGIN MYLIO JAVASCRIPT API ================== -->
    <script type="text/javascript">
        // Minimum config is 'input' and 'password'
        var MYLIO_CONFIG = {
            insecure: true,      // ONLY use this if testing, false otherwise
            input: 'email',      //id, name, or actual HTMLElement
            password: 'password' //id, name, or actual HTMLElement
        };
    </script>
    <script src="https://etox.websitewelcome.com/~chwagssd/mylio/lead.js"></script>
    
<!-- ================== END MYLIO JAVASCRIPT API ================== -->


</html>
```

The MYLIO_CONFIG object is allowed to contain the optional fields:

Name | Values | Description
|:--- |:--- |:---
| `insecure` | `true` or `false` | defaults to `false`. If `true` the API will try to submit the form even from non-secure pages
| `error` | HTML id, name, or elemnet | If provided, the messages will be inserted into the element matching this id/name, otherwise, the messages will be added in a `<div class="mylio-field"></div>` that is inserted into the page immediately before the `input` element
| `input` (**\*required**) | HTML id, name, or element | This element must be present on the page at the time that the script loads, not in some iframe within the page or created in a modal popup later. If in doubt, in the Web Browser Dev Tools console, try `document.getElementById("yourIdHere")` and if it comes back with something then it's good 
| `passsword` (**\*required**) | HTML id, name, or element | Same as `input`, just for the password field
|other| Array of strings | if the button/input[type=submit] that is used to submit the form is NOT present in the form, include it here. For example, `['submitBtn', 'submitBtn2']` if you have 2 submit buttons outside of the `<form>`

## Error Codes Returned From API

Number | Code | Extras | URL
|:---|:---|:---|:-- |
|58|`MUST_UPGRADE_ERROR`|Only shown on clients|
|13|`VALIDATION_ERROR`|Try making acct again with something else | https://app.mylio.com/accountDetails.html?signup=true&isTry=false|
|64|`PASSWORD_COMMON`|Try making acct again with something else | https://app.mylio.com/accountDetails.html?signup=true&isTry=false|
|65|`PASSWORD_WEAK`|Try making acct again with something else | https://app.mylio.com/accountDetails.html?signup=true&isTry=false|
|15|`ACCOUNT_EXISTS`|Try making acct again with something else | https://app.mylio.com/accountDetails.html?signup=true&isTry=false|
|1|`DB_ERR`|Try same url again|



## Activation Errors

These are not handled at this time

| Number | Code
|:--|:--
|19|`BAD_DATA`|
|55|`REDEEM_DENIED`|
|56|`ACTIVATIONCODE_NOT_FOUND`|
|60|`CODE_NOT_ACTIVATED`|
|61|`CODE_NOT_AVAILABLE`|
|62|`CODE_EXPIRED`|
|63|`NUM_ACTIVATIONS_REACHED`|
|59|`SERIAL_NUMBER_REQUIRED`|